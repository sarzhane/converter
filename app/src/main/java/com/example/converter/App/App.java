package com.example.converter.App;

import android.app.Application;

import com.example.converter.presentation.activity.Converter;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Converter.getInstance();
    }
}
