package com.example.converter.presentation.activity;

public class Presenter implements Contract.Presenter {
    private Contract.View view;

    public Presenter() {

    }

    @Override
    public void eventOperation(String val) {
        view.showResult(Converter.getInstance().convertValue(val));
    }

    @Override
    public void startView(Contract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null) view = null;
    }
}
