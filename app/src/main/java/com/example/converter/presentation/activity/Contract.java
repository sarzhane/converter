package com.example.converter.presentation.activity;

import com.example.converter.presentation.base.BaseActivityContract;
import com.example.converter.presentation.base.BasePresenter;

public interface Contract {
    interface View extends BaseActivityContract {
        void showResult(String val);
    }

    interface Presenter extends BasePresenter<View> {
        void eventOperation(String val);
    }
}
