package com.example.converter.presentation.base;

public interface BasePresenter<V> {
    void startView(V view);
    void detachView();
}
