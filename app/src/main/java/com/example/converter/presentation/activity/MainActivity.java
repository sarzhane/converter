package com.example.converter.presentation.activity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.converter.R;
import com.example.converter.presentation.base.BaseActivity;
import com.example.converter.presentation.base.BasePresenter;

public class MainActivity extends BaseActivity implements Contract.View {
    private Contract.Presenter presenter;
    TextView textViewResult;
    EditText editTextValue;
    Button convert;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
    }

    @Override
    protected void initView() {
        presenter = new Presenter();
        textViewResult = findViewById(R.id.textViewResult);
        editTextValue = findViewById(R.id.editTextValue);
        convert = findViewById(R.id.buttonConvert);
    }

    @Override
    protected void onDestroyView() {

    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    public void Convert(View view) {
        presenter.eventOperation(editTextValue.getText().toString());
    }

    @Override
    public void showResult(String val) {
        textViewResult.setText(val);
    }
}
