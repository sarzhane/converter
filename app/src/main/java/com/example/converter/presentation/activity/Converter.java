package com.example.converter.presentation.activity;

public class Converter {
    private static Converter instance;

    private Converter() {
    }

    public static synchronized Converter getInstance() {
        if (instance == null) {
            instance = new Converter();
        }
        return instance;
    }

    public String convertValue(String val) {
        char symbols[] = val.toCharArray();
        StringBuilder answer = new StringBuilder();
        for (int i = symbols.length - 1; i >= 0; i--) {
            answer.append(symbols[i]);

        }
        return answer.toString();
    }
}
